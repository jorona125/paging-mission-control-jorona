import datetime
import json
import sys

file_name = sys.argv[1]
plus_5 = datetime.timedelta(minutes=+5)

def check_red(entry):
    """
    Check the severity level of an entry and return a dictionary with relevant information.

    Args:
        entry (list): A list containing the entry information.

    Returns:
        dict: A dictionary containing the following keys:
            - "SatelliteId": The ID of the satellite.
            - "severity": The severity level of the entry.
            - "Component": The component of the entry.
            - "Time Stamp": The timestamp of the entry.
            - "Out Time": The formatted timestamp in UTC.

    """
    test = datetime.datetime.strptime(entry[0],'%Y%d%m %H:%M:%S.%f' )
    out_date = test.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    component = entry[-1].strip()
    severity = "Ok"
    
    if component == "TSTAT":
        if float(entry[-2]) >= float(entry[2]):
            severity = "red-high-limit"
        elif float(entry[-2]) <= float(entry[5]):
            severity = "red-low-limit"
    elif component == "BATT":
        if float(entry[-2]) >= float(entry[2]):
            severity = "red-high-limit"
        elif float(entry[-2]) <= float(entry[5]):
            severity = "red-low-limit"  
    return {
       "SatelliteId": entry[1],
        "severity": severity,
        "Componet": component,
        "Time Stamp": test.strftime('%Y%m%d %H:%M'),
        "Out Time": out_date
    }
  
import datetime

def check_time(times, delta):
    """
    Check the time stamps in the given list and return a list of alert messages.

    Args:
        times (list): A list of dictionaries containing time stamp information.
        delta (datetime.timedelta): The time interval to consider for alerts.

    Returns:
        list: A list of dictionaries representing alert messages.

    """
    tstat_count = 0
    batt_count = 0
    start_time = datetime.datetime.strptime(times[0]["Time Stamp"], '%Y%m%d %H:%M')
    alert_message = []

    for i in times:
        stamp = datetime.datetime.strptime(i["Time Stamp"], '%Y%m%d %H:%M')
        if i is None:
            pass
        if (stamp == start_time or i["severity"] == "Ok"):
            continue
        elif stamp <= start_time + delta and i["Componet"] == "TSTAT":
            tstat_count += 1
            if tstat_count == 3:
                i["Time Stamp"] = i.pop("Out Time")
                alert_message.append(i)
                tstat_count = 0
                start_time = stamp
        elif stamp <= start_time + delta and i["Componet"] == "BATT":
            batt_count += 1
            if batt_count == 3:
                i["Time Stamp"] = i.pop("Out Time")
                alert_message.append(i)
                batt_count = 0
                start_time = stamp
        else:
            continue

    return alert_message
        
def main ():
    output_list = []
    with open(file_name, "r") as data:
        for line in data:
            entry = line.split("|".strip())
            # #print(entry)
            output = check_red(entry)
            if output is not None:
                output_list.append(output)
        return(json.dumps(check_time(output_list, plus_5), 
                         skipkeys=True,
                         allow_nan=True,
                         indent = 6))

if __name__ == "__main__":
    print(main())