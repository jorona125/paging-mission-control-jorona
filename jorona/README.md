Pagin Mission Control By Jose Orona

My solution uses the following builtin Python module:
```
import datetime
import json
import sys

```

Use case : $ python alert.py <file name>

In my solution i have three fucntions 

This funcion compares the "Raw Value" to the red-high and red-low limits. if it meets either of the limits it will return the entry in a dictionary. 
```
check_red(entry)
```
returns:
```
{
       "SatelliteId": entry[1],
        "severity": severity,
        "Componet": component,
        "Time Stamp": test.strftime('%Y%m%d %H:%M'),
        "Out Time": out_date
    }
```

This function accepts two arguements a timestamp in string format and a datetime.timedelta(minuets=+5). the time delta is used to create time limit for the alert notifications.  
```
#sampletime delta:
plus_5 = datetime.timedelta(minutes=+5)

check_time(times, delta)
```
if the componet has a count of 3 within a 5 minuet period it will be added to a list "alert_messages". in the following format
```
      {
            "SatelliteId": "1001",
            "severity": "red-high-limit",
            "Componet": "TSTAT",
            "Time Stamp": "2018-01-01T23:16:40.001000Z"
      }
```

finally, our main function is use to read a file. for each line of the file are split at the " | " delimeter and passed into our "check_red()", and stored as "output" and appended to list "ouput_list". "output_list" is passed to our "check_time()" funciton with the time delta of "plus_5". the output of "check_time()" then retunred using json.dumps.



